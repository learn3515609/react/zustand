import React from 'react';
import { UsersAdd } from './modules/users/users-add';
import { UsersList } from './modules/users/users-list';

function App() {
  return <>
    <UsersList />
    <UsersAdd />
  </>
}

export default App;

import { create, StateCreator } from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import { immer } from 'zustand/middleware/immer';

export const createStore = <T>(
  storeName: string,
  store: StateCreator<T, [["zustand/devtools", never], ["zustand/immer", never]]>
) => create<T>()(
  persist(
    devtools(
      immer(
        store
      ) 
    )
  , {name: storeName, version: 1})
)
import React, { useEffect } from 'react';
import { useUsersStore } from './users-store';

export const UsersList = () => {
  const { users, fetchUsers, isLoading } = useUsersStore(state => ({
    users: state.users,
    fetchUsers: state.fetchUsers,
    isLoading: state.isLoading
  }));

  useEffect(() => {
    fetchUsers();
  },[])

  if(isLoading) return <p>Loading...</p>;

  return <ul>
    {users.map(item => <li key={item.id}>{item.name}</li>)}
  </ul>;
}
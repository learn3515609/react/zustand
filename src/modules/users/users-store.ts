import { delay } from '../../helpers/delay';
import { createStore } from '../../helpers/store';

interface IUser {
  id: number;
  name: string;
}

type TUsersState = {
  users: IUser[],
  isLoading: boolean,
  isLoaded: boolean,
  addUser: (user: IUser) => void,
  fetchUsers: () => void
}

export const useUsersStore = createStore<TUsersState>('users', set => ({
  users: [],
  isLoading: false,
  isLoaded: false,

  addUser: (user: IUser) => set(state => {
    state.users.push(user)
  }),

  fetchUsers: async() => {
    set(state => ({isLoading: true, isLoaded: false}));
    await delay(2000);
    fetch('./users.json')
      .then(data => data.json())
      .then((data: IUser[]) => {
        set(state => ({
          users: data,
          isLoading: false, isLoaded:true
        }))
      })
  },
}))
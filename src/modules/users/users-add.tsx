import React from 'react';
import { useUsersStore } from './users-store';

export const UsersAdd = () => {
  const addUser = useUsersStore(state => state.addUser);

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    addUser({
      id: Date.now(),
      name: 'new user'
    })
  }

  return <form onSubmit={onSubmit}>
    <button>Add user</button>
  </form>;
}